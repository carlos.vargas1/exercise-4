package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class RestaurantsPage {
    private WebDriver driver;

    private String resultsByXpathPart1 = "//li[descendant::span[contains(@class, 'ranking')]]";
    private String resultsByXpathPart2 = "//h3/a";
    private By dropdownBy = By.xpath("//*[contains(@class,'dropdown') and contains(text(),'Recomendados')]");
    private String dropdwnOptionByPart1 = "//*[@id='drop1']//a[contains(.,'";
    private String getDropdwnOptionByPart2 = "')]";
    private String filterPart1 = "//span[text()='";
    private String filterPart2 = "']";
    private String ratingPart= "//span/i[contains(@class, 'rating-points')]";
    private By firstResultBy = By.xpath("(" + resultsByXpathPart1 + ")[1]//a[contains(@class, 'arrivalName')]");
    private By btnContinueLaterBy = By.xpath("(//*[@id='lnkConfirmPop'])[2]");
    private List <String> resultList;
    private List <String> starList;

    public RestaurantsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void calculateResults(){
        List<WebElement> list = driver.findElements(By.xpath(resultsByXpathPart1));
        int size = list.size();
        resultList = new ArrayList<>();
        starList = new ArrayList<>();

        for(int i=1; i<=size; i++){
            String textName = driver.findElement(By.xpath("(" + resultsByXpathPart1 + ")[" + i + "]" + resultsByXpathPart2) ).getText();
            String rating = driver.findElement(By.xpath("(" + ratingPart + ")[" + i + "]")).getText();
            resultList.add(textName);
            starList.add(rating);
        }
    }

    public WebElement getBtnContinueLater(){
        try{
            return driver.findElement(btnContinueLaterBy);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<String> getResultList() {
        return resultList;
    }

    public List<String> getStarList() {
        return starList;
    }

    public WebElement getDropDown(){
        return driver.findElement(dropdownBy);
    }

    public WebElement getDropDownOption(String option){
        By getDropDownOptionBy = By.xpath(dropdwnOptionByPart1 + option + getDropdwnOptionByPart2);
        return driver.findElement(getDropDownOptionBy);
    }

    public WebElement getFilter(String option){
        By getFilterOptionBy = By.xpath(filterPart1 + option + filterPart2);
        return driver.findElement(getFilterOptionBy);
    }

    public WebElement getFirstResult(){
        return driver.findElement(firstResultBy);
    }
}

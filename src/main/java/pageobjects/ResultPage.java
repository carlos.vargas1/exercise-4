package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class ResultPage {
    private WebDriver driver;

    private By distanceBy = By.className("distance");
    private By deliveryTimeBy = By.className("deliveryTime");
    private By ratingPointsBy = By.xpath("//span[contains(@class, 'new_rating box_split_review_04')]");
    private By seeAllBy = By.xpath("//a[contains(@class, 'btn_see_all')]");
    private String baseComment = "//ul[@id='reviewList']//li";
    private String baseAuthor = "//div[@itemprop='author']";
    private String baseRating = "//header/i[contains(@class, rating-points)]";
    private String baseDate = "//p[@itemprop='datePublished']";

    public ResultPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getDistance(){
        return driver.findElement(distanceBy);
    }

    public WebElement getDeliveryTime(){
        return driver.findElement(deliveryTimeBy);
    }

    public List<WebElement> getRatingPoints(){
        return  driver.findElements(ratingPointsBy);
    }

    public WebElement getSeeAll(){
        return driver.findElement(seeAllBy);
    }

    public List<WebElement> getCommentByIndex(int index){
        By ratingBy = By.xpath("(" + baseComment + ")[" + index + "]" + baseRating);
        By authorBy = By.xpath("(" + baseComment + ")[" + index + "]" + baseAuthor);
        By dateBy = By.xpath("(" + baseComment + ")[" + index + "]" + baseDate);

        List<WebElement> list = new ArrayList<>();
        list.add(driver.findElement(ratingBy));
        list.add(driver.findElement(authorBy));
        list.add(driver.findElement(dateBy));

        return list;
    }
}

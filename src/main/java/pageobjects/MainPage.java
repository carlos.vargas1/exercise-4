package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private WebDriver driver;

    private By dropdownBy = By.id("selectCity_chosen");
    private By restaurantBy = By.id("optional");
    private By btnSearchBy = By.id("search");
    private String cityBy1 = "//li[text()[contains(.,'";
    private String cityBy2 = "')]]";
    private By addressBy = By.id("address");
    private By btnConfirmBy = By.id("confirm");
    private By mapBy = By.className(("gm-style"));
    private String url = "https://www.pedidosya.com.uy/";


    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getDropDown(){
        return driver.findElement(dropdownBy);
    }

    public WebElement getRestaurant(){
        return driver.findElement(restaurantBy);
    }

    public WebElement getBtnSearchBy(){
        return driver.findElement(btnSearchBy);
    }

    public WebElement getCityBy(String city){
        By cityBy = By.xpath(cityBy1 + city + cityBy2);
        return driver.findElement(cityBy);
    }

    public WebElement getAddressBy(){
        return driver.findElement(addressBy);
    }

    public WebElement getBtnConfirmBy(){
        return driver.findElement(btnConfirmBy);
    }

    public WebElement getMap(){
        return driver.findElement(mapBy);
    }

    public String getUrl(){
        return url;
    }
}
package resources;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Base {
    public static WebDriver driver;
    public Properties prop;

    public WebDriver initializeDriver() throws IOException {
        String propertiesPath = System.getProperty("user.dir")+"/src/main/java/resources/data.properties";
        prop= new Properties();
        FileInputStream fis = new FileInputStream(propertiesPath);
        prop.load(fis);
//        String browserName=prop.getProperty("browser"); //use this if using document
        String browserName=System.getProperty("browser"); //use this if using maven
        System.out.println(browserName);

        if(browserName.contains("chrome")){
            System.setProperty("webdriver.chrome.driver", "//Users//calvario//Development_Tools//chromedriver//chromedriver");
            driver= new ChromeDriver();
        }

        else if (browserName.contains("firefox")){
            FirefoxOptions options = new FirefoxOptions();
            options.setProfile(new FirefoxProfile());
            options.addPreference("dom.webnotifications.enabled", false);
            options.addPreference("browser.privatebrowsing.autostart", true);

            System.setProperty("webdriver.gecko.driver", "//Users//calvario//Development_Tools//geckodriver//geckodriver");
            driver = new FirefoxDriver(options);

        }

        driver.manage().window().maximize();
        driver.manage().window().fullscreen();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        return driver;
    }

    public String getScreenShotPath(String testCaseName, WebDriver driver) throws IOException {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String destinationFile = System.getProperty("user.dir")+"/reports/"+testCaseName+".png";
        FileUtils.copyFile(source,new File(destinationFile));
        return destinationFile;
    }
}

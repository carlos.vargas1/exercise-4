package pedidos;

import com.google.common.base.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.RestaurantsPage;
import pageobjects.ResultPage;
import resources.Base;

import javax.xml.transform.Result;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Ex4 extends Base {
    WebDriver driver;
    MainPage mainPage;
    RestaurantsPage restaurantsPage;
    ResultPage resultPage;
    List<String> auxList;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        mainPage = new MainPage(driver);
        restaurantsPage = new RestaurantsPage(driver);
        resultPage = new ResultPage(driver);
    }

    @Test
    public void test() throws InterruptedException {
//        Go to https://www.pedidosya.com.uy/
        driver.get(mainPage.getUrl());
//        Set any street and search for "Pizza"
        mainPage.getDropDown().click();
        mainPage.getCityBy("Mercedes").click();
        mainPage.getRestaurant().sendKeys("Pizza");
        mainPage.getAddressBy().sendKeys("main street 123");
        mainPage.getBtnSearchBy().click();
//        Click Search button (Confirm the street, do not move the location)
        Thread.sleep(1500);
        mainPage.getMap().click();
        mainPage.getBtnConfirmBy().click();
//        Report total no. of Search results with no. of results in the current page
        restaurantsPage.calculateResults();
        System.out.println("desordenado");
        printResults(restaurantsPage.getResultList());
//        Select a random Filter (Express, Sellos, etc)
        restaurantsPage.getFilter("Cupón").click();
//        Verify the number of results is correct
        restaurantsPage.calculateResults();
        System.out.println(restaurantsPage.getResultList().size());
//        Sort by: "Alfabeticamente"
        deepCopy(restaurantsPage.getResultList());
        auxList = new ArrayList<>(restaurantsPage.getResultList());
        Collections.sort(auxList);
        System.out.println("ordenado por código");
        printResults(auxList);
        restaurantsPage.getDropDown().click();
        restaurantsPage.getDropDownOption("Alfabéticamente").click();
        restaurantsPage.calculateResults();
        System.out.println("ordenado alfabéticamente desde la página");
        printResults(restaurantsPage.getResultList());
//        Verify the sort is correct
        Verify.verify(auxList.equals(restaurantsPage.getResultList()));
//        Report total no. of Search results with no. of results in the current page
        System.out.println("size: " + auxList.size());
//        Report the star rating of each of the results in the first result page
        printStarResults(restaurantsPage.getResultList(), restaurantsPage.getStarList());
//        Go into the first result from the search results
        restaurantsPage.getFirstResult().click();
        Thread.sleep(2000);
        if(restaurantsPage.getBtnContinueLater() != null){
            restaurantsPage.getBtnContinueLater().click();
        }
//        Log all critical information of the selected restaurant:
//        Details (distance, open hours, rating points, etc)
        System.out.println(resultPage.getDistance().getText());
        System.out.println(resultPage.getDeliveryTime().getText());
        printRatings(resultPage.getRatingPoints());
//        First 3 customer reviews (name, date, rating points, etc)
        resultPage.getSeeAll().click();
        for(int i=1; i<4; i++){
            printReviews(resultPage.getCommentByIndex(i));
        }

    }

    @AfterTest
    public void closeDriver(){
        driver.close();
    }

    private void printResults(List<String> list){
        int index=1;
        for (String element:list) {
            System.out.println(index + ". " + element);
            index++;
        }
        System.out.println("");
    }

    private void deepCopy(List<String> source){
        ArrayList<String> copy = new ArrayList<>();
        for(String element: source){
            copy.add(element);
        }
        Collections.sort(copy, String.CASE_INSENSITIVE_ORDER);
    }

    private void printStarResults(List<String> list1, List<String> list2){
        for(int i=0; i<list1.size(); i++){
            System.out.println(list1.get(i) + "\t" + list2.get(i));
        }
    }

    private void printRatings(List<WebElement> list){
        for(int i=0; i<list.size(); i++){
            switch (i){
                case 0:
                    System.out.print("velocidad:\t");
                    break;
                case 1:
                    System.out.print("comida:\t");
                    break;
                case 2:
                    System.out.print("servicio:\t");
                    break;
                default:
                    System.out.print("error:\t");
            }
            System.out.println(list.get(i).getText());
        }
        System.out.println("");
    }

    private void printReviews(List<WebElement> list){
        for(int i=0; i<list.size(); i++){
            switch (i){
                case 0:
                    System.out.print("rating:\t");
                    break;
                case 1:
                    System.out.print("author:\t");
                    break;
                case 2:
                    System.out.print("date:\t");
                    break;
                default:
                    System.out.print("error:\t");
            }
            System.out.println(list.get(i).getText());
        }
        System.out.println("");
    }
}
